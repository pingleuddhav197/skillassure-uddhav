// Series 1,-2,3,-4,......,N lab5
package series;
import java.util.*;

public class Series {
	public static void main(String args[])
	{
		//declaration
		int i,lim;
		
		//user initialization
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the limit number:");
		lim=s.nextInt();
		
		//main logic and series display
		for(i=1;i<=lim;i++)
		{
			if(i%2==0)
				System.out.println("-"+i+",");
			else
				System.out.println(i+",");
			
		}
	
	
	
	}
}
