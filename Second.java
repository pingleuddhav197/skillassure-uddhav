//lab2 swapping of numbers
package second;
import java.util.*;

public class Second {

	public static void main(String[] args) {
		//Declaration
		int x,y,swap;
		
		//initialize x,y
		Scanner s=new Scanner(System.in);
		System.out.println("enter x:");
		x=s.nextInt();
		System.out.println("enter y:");
		y=s.nextInt();
		
		//original number display
		System.out.println("original nos are:"+x+y);
		
		//number swapping logic
		swap=x;
		x=y;
		y=swap;
		
		//swapped number display
		System.out.println("changed numbers are:"+x+y);
		
		

	}

}
