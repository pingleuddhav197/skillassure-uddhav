//lab3 Largest of 3 numbers
import java.util.*;
public class Large 
{

	public static void main(String[] args) {
		//declaration
		int a,b,c,large,slarge;
		Scanner s=new Scanner(System.in);
		
		//initialization
		System.out.println("Enter first number:");
		a=s.nextInt();
		System.out.println("Enter second number:");
		b=s.nextInt();
		System.out.println("Enter Third number:");
		c=s.nextInt();
		
		//Main logic
		if(a>b && a>c)
		{
			large=a;
			if(b>c)
				slarge=b;
			else
				slarge=c;
		}
		else if(b>c && b>a)
		{
			large=b;
			if(a>c)
				slarge=a;
			else
				slarge=c;
		}
		else
		{
			large=c;
			if(b>a)
				slarge=b;
			else
				slarge=a;
		}
		
		System.out.println("The largest no is:"+large+" and second largest no is:"+slarge);

	}

}
