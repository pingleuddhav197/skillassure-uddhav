//lab6 series 1,-2,6,-15,31,.......,N
package series;
import java.util.*;
import java.math.*;
public class Series2 {

	public static void main(String[] args) {
		//declration and initialization
		int i,n,sign=1;
		int sum = 1;
		
		//user defined limit
		System.out.println("Enter the series limit: ");
		Scanner s=new Scanner(System.in);
		n=s.nextInt();
		//looping and display
		for( i=1;i<=n;i++)
		{
			System.out.print(sum*sign+",");
			sign = sign * -1;
			sum =sum + (i*i);		
		}
		System.out.println();

	}

}
